# Read Example

This example reads and displays text from a serial port.

## Build

Open platform IO terminal in this directory and build  for your environment using the following command.
```sh
pio run -e <environment>
```

**eg:**
```sh
pio run -e read_linux_x86_64
```

## Run

Run the application

```sh
.pio/build/<environment>/program <serial_port> <baudrate>
```
**Example:**  
```sh
.pio/build/read_linux_x86_64/program /dev/ttyUSB0 9600
```

### Run Using Virtual Serial Port

> This example assumes `linux_x86_64` environment and a baudrate of `9600`

1. Create virtual serial ports using `socat`
   
   if socat is not available in your system, install it:
   
   ```sh
   # this is for ubuntu
   sudo apt install socat
   ```
   
   Open a terminal and type:
   
   ```sh
   socat -d -d pty,raw,echo=0 pty,raw,echo=0
   ```
   
   The above command produces an output similar to this:
   
   ```
   2021/08/02 00:52:06 socat[4971] N PTY is /dev/pts/5
   2021/08/02 00:52:06 socat[4971] N PTY is /dev/pts/6
   2021/08/02 00:52:06 socat[4971] N starting data transfer loop with FDs [5,5] and [7,7]
   ```
   
   Here `/dev/pts/5` & `/dev/pts/6` are the virtual ports, which are connected to each other
   
   > if your portnames are different modify below steps accordingly

2. Run the program using one of the virtual ports (I am using `/dev/pts/5`) 
   
   ```sh
   .pio/build/read_linux_x86_64/program /dev/pts/5 9600
   ```

3. Open another command prompt and communicate with program via picocom
   
   ```sh
   picocom /dev/pts/6 --baud 9600 --omap crlf --echo
   ```
