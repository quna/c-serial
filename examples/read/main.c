#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>

#include "serial.h"

#define PORT_ENV "PORT"
#define PORT_MAXLEN 15

#define BAUDRATE_ENV "BAUD"
#define BAUDRATE_MAXLEN 7 // 2,000,000
#define BAUDRATE_DEFAULT 9600

#define DATA_BITS 8
#define PARITY 0
#define STOP_BITS 1

#define READ_BUFF_SIZE 256

typedef struct CONFIG_STRUCT
{
  serial_mode_t mode;
  char port[PORT_MAXLEN + 1];
} config_t;

bool get_configs(int argc, char *argv[], char **envp, config_t *config);
bool parse_env_variable(char **envp, char *variable, char *val, int max_len);
bool parse_cmd_args(int argc, char *argv[], config_t *config);

int main(int argc, char *argv[], char **envp)
{
  config_t config;
  if (!get_configs(argc, argv, envp, &config))
  {
    fprintf(stderr, "Usage\t: %s [<port> [<baudrate>]]\n"
                    "\t port and baudrate can also be configured using PORT and BAUDRATE environment variables respectively.\n ",
            argv[0]);
    exit(1);
  }

  int fd;
  fd = serial_begin(config.port, config.mode);
  if (fd < 0)
  {
    fprintf(stderr, "Failed to open port");
    exit(2);
  }

  char buff[READ_BUFF_SIZE];
  int num_bytes_read;

  while (1)
  {
    if (serial_available(fd) > 0)
    {

      memset(&buff, 0, READ_BUFF_SIZE);
      // We are reading ASCII string so we need last byte for null termination
      num_bytes_read = serial_read(fd, &buff, READ_BUFF_SIZE - 1);
      if (num_bytes_read < 0)
      {
        printf("Error reading: %s", strerror(errno));
        break;
      }
      printf("%s", buff);
      fflush(stdout);
    }
    usleep(1000); // prevents program from consuming 100% CPU
  }

  return serial_end(fd);
}

bool parse_env_variable(char **envp, char *variable, char *val, int max_len)
{
  size_t variable_len = strlen(variable);
  char prefix[variable_len + 2];
  strncpy(prefix, variable, variable_len + 1);
  strcat(prefix, "=");

  while (*envp != NULL)
  {
    if (strncmp(*envp, prefix, strlen(prefix)) == 0)
    {
      strncpy(val, *(envp) + strlen(prefix), max_len + 1);
      return true;
    }
    envp++;
  }

  return false;
}

bool parse_cmd_args(int argc, char *argv[], config_t *config)
{
  if (argc > 1)
  {
    strncpy(config->port, argv[1], PORT_MAXLEN + 1);
  }
  if (argc > 2)
  {
    int baudrate = atoi(argv[2]);
    if (baudrate == 0)
    {
      return false;
    }
    config->mode.baud_rate = baudrate;
  }

  return true;
}

bool get_configs(int argc, char *argv[], char **envp, config_t *config)
{
  // environment variables
  char baud_rate_var[BAUDRATE_MAXLEN + 1] = "";
  parse_env_variable(envp, BAUDRATE_ENV, baud_rate_var, BAUDRATE_MAXLEN);
  int baudrate = atoi(baud_rate_var);
  if (baudrate == 0)
  {
    baudrate = BAUDRATE_DEFAULT;
  }

  char port[PORT_MAXLEN + 1] = "";
  parse_env_variable(envp, PORT_ENV, port, PORT_MAXLEN);

  strncpy(config->port, port, PORT_MAXLEN + 1);
  config->mode = (serial_mode_t){.baud_rate = baudrate,
                                 .data_bits = DATA_BITS,
                                 .parity = eSERIAL_PARITY_NONE,
                                 .stop_bits = STOP_BITS};

  if (!parse_cmd_args(argc, argv, config))
  {
    return false;
  }

  if (config->port[0] == '\0')
  {
    return false;
  }

  return true;
}
