#include "err.h"
#include <stdio.h>
#include <stdlib.h>

void _error_check_failed_without_abort(const char *file, int line, const char *expression)
{
    fprintf(stderr, "ERROR!! Check failed at %p\n",
            __builtin_return_address(0));
    fprintf(stderr, "file: \"%s\" line %d\nexpression: %s\n", file, line, expression);
}

void _error_check_failed(const char *file, int line, const char *expression)
{
    _error_check_failed_without_abort(file, line, expression);
    abort();
}
