#include <fcntl.h>
#include <stdbool.h>
#include <stdio.h>
#include <termio.h>
#include <unistd.h>

#include "serial.h"

int serial_begin(const char *port, serial_mode_t mode)
{
  struct termios options;
  int fd;

  // Open device
  fd = open(port, O_RDWR);
  // If the device is not open, return eror value
  if (fd < 0)
  {
    return fd;
  }

  // Get the current options of the port
  tcgetattr(fd, &options);

  // clear or set parity bit
  switch (mode.parity)
  {
  case eSERIAL_PARITY_ODD:
    options.c_cflag |= PARENB;
    options.c_cflag |= PARODD;
    break;

  case eSERIAL_PARITY_EVEN:
    options.c_cflag |= PARENB;
    options.c_cflag &= ~PARODD;
    break;

  case eSERIAL_PARITY_NONE:
    options.c_cflag &= ~PARENB;

  default:
    break;
  }

  // Set number of stop bits
  switch (mode.stop_bits)
  {
  case 1:
    // Clear stop field, only one stop bit used in communication
    options.c_cflag &= ~CSTOPB;
    break;
  case 2:
    // Set stop field, two stop bits used in communication
    options.c_cflag |= CSTOPB;
    break;
  default:
    printf("ERROR: Invalid number of stop bits");
    return -2;
  }

  // set databits count
  options.c_cflag &= ~CSIZE; // Clear all the size bits
  switch (mode.data_bits)
  {
  case 8:
    options.c_cflag |= CS8; // 8 bits per byte
    break;
  case 7:
    options.c_cflag |= CS7; // 7 bits per byte
    break;
  case 6:
    options.c_cflag |= CS6; // 6 bits per byte
    break;
  case 5:
    options.c_cflag |= CS5; // 5 bits per byte
    break;
  default:
    printf("ERROR: Invalid databits count");
    return -3;
  }

  // set speed (Bauds)
  speed_t speed;
  switch (mode.baud_rate)
  {
  case 110:
    speed = B110;
    break;
  case 300:
    speed = B300;
    break;
  case 600:
    speed = B600;
    break;
  case 1200:
    speed = B1200;
    break;
  case 2400:
    speed = B2400;
    break;
  case 4800:
    speed = B4800;
    break;
  case 9600:
    speed = B9600;
    break;
  case 19200:
    speed = B19200;
    break;
  case 38400:
    speed = B38400;
    break;
  case 57600:
    speed = B57600;
    break;
  case 115200:
    speed = B115200;
    break;
  default:
    printf("ERROR: invalid baudrate");
    return -4;
  }
  // Set the baud rate
  cfsetispeed(&options, speed);
  cfsetospeed(&options, speed);

  // Flow control
  // Explicitly disable RTS/CTS flow control
  options.c_cflag &= ~CRTSCTS;

  // Turn on READ & ignore ctrl lines (CLOCAL = 1)
  options.c_cflag |= CREAD | CLOCAL;

  // Disable canonical mode ( enable non-canonical/raw mode)
  options.c_lflag &= ~ICANON;

  // Disable echo
  options.c_lflag &= ~ECHO;   // Disable echo
  options.c_lflag &= ~ECHOE;  // Disable erasure
  options.c_lflag &= ~ECHONL; // Disable new-line echo

  // Disable interpretation of INTR, QUIT and SUSP (signal chars)
  options.c_lflag &= ~ISIG;

  // Disable software flow control
  options.c_iflag &= ~(IXON | IXOFF | IXANY);

  // Disable Special Handling Of Bytes On Receive
  options.c_iflag &=
      ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

  // Output Modes
  options.c_oflag &= ~OPOST; // Prevent special interpretation of output bytes
  options.c_oflag &=
      ~ONLCR; // Prevent conversion of newline to carriage return/line feed
  // options.c_oflag &= ~OXTABS; // Prevent conversion of tabs to spaces (NOT
  // PRESENT IN LINUX) options.c_oflag &= ~ONOEOT; // Prevent removal of C-d
  // chars (0x004) in output (NOT PRESENT IN LINUX)

  // Blocking configutation

  options.c_cc[VTIME] = 10; // Wait for up to 1s (10 deciseconds)
  options.c_cc[VMIN] = 5;   // Wait for up to 5 bytes
  // Block until either VMIN characters have been received, or VTIME after first
  // character has elapsed. The timeout for VTIME does not begin until the first
  // character is received.

  // Save the settings
  if (tcsetattr(fd, TCSANOW, &options) != 0)
  {
    return -5;
  }
  return fd;
}

int serial_end(int fd) { return close(fd); }

int serial_available(int fd)
{
  int bytes_available;
  ERROR_CHECK(-1 == ioctl(fd, FIONREAD, &bytes_available));
  return bytes_available;
}

int serial_write(int fd, const void *buf, int count)
{
  return write(fd, buf, count);
}

int serial_read(int fd, void *buf, int count) { return read(fd, buf, count); }

int get_modem_bits(int fd)
{
  int modem_bits = 0;
  ERROR_CHECK(-1 == ioctl(fd, TIOCMGET, &modem_bits));
  return modem_bits;
}

bool get_modem_bit(int fd, int modem_line)
{
  return get_modem_bits(fd) & modem_line;
}

void set_modem_bit(int fd, int modem_line, bool status)
{
  int modem_bits = get_modem_bits(fd);
  if (status)
  {
    modem_bits |= modem_line;
  }
  else
  {
    modem_bits &= ~modem_line;
  }
  ERROR_CHECK(-1 == ioctl(fd, TIOCMSET, &modem_bits));
}

void set_DTR(int fd, bool status) { set_modem_bit(fd, TIOCM_DTR, status); }

bool is_DTR(int fd) { return get_modem_bit(fd, TIOCM_DTR); }

void set_RTS(int fd, bool status) { set_modem_bit(fd, TIOCM_RTS, status); }

bool is_RTS(int fd) { return get_modem_bit(fd, TIOCM_RTS); }

bool is_RI(int fd) { return get_modem_bit(fd, TIOCM_RNG); }

bool is_DCD(int fd) { return get_modem_bit(fd, TIOCM_CAR); }

bool is_CTS(int fd) { return get_modem_bit(fd, TIOCM_CTS); }

bool is_DSR(int fd) { return get_modem_bit(fd, TIOCM_DSR); }

int serial_flush_reciever(int fd) { return tcflush(fd, TCIFLUSH); };
int serial_flush_transmitter(int fd) { return tcflush(fd, TCOFLUSH); };
int serial_flush(int fd) { return tcflush(fd, TCIOFLUSH); };
