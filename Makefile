CC = gcc
CFLAGS = -Wall 

LIB_NAME = serial

SRC_DIR = src
SRCS = $(wildcard $(SRC_DIR)/*.c)
BUILD_DIR = build
OBJS = $(patsubst %.c,%.o,$(SRCS))
OBJS := $(patsubst $(SRC_DIR)/%,$(BUILD_DIR)/%,$(OBJS))

INC_DIR = include
CFLAGS += -I$(INC_DIR)

all: shared static

deps := $(patsubs %.o,%.d,$(objs))
-include $(deps)
DEPFLAGS = -MMD -MF $(@:.o=.d)

$(BUILD_DIR)/%.o:$(SRC_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) $(CFLAGS) -c $^ -o $@ $(DEPFLAGS)

$(BUILD_DIR)/lib$(LIB_NAME)static.a:$(OBJS)
	@mkdir -p $(@D) 
	ar rcs $@ $^

static: $(BUILD_DIR)/lib$(LIB_NAME)static.a

$(BUILD_DIR)/lib$(LIB_NAME).so:$(SRCS)
	@mkdir -p $(@D) 
	$(CC) $(CFLAGS) -fPIC -shared -o $@ $^

shared: $(BUILD_DIR)/lib$(LIB_NAME).so

clean:
	rm -rf build

.PHONY: all shared static clean