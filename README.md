# c-serial

POSIX C libray for serial communication

## [Exampels](./examples/)

Instructions for building and running examples are provided in README files inside corresponding directories.

## References
* [Serial Programming Guide for POSIX Operating Systems _@Michael R. Sweet_](https://www.cmrr.umn.edu/~strupp/serial.html)
* [Linux Serial Ports Using C/C++ _@Geoffrey Hunter_](https://blog.mbedded.ninja/programming/operating-systems/linux/linux-serial-ports-using-c-cpp/)
* https://github.com/imabot2/serialib
* https://github.com/bugst/go-serial
