#ifndef CSERIAL_SERIAL_H
#define CSERIAL_SERIAL_H

#include "err.h"
#include <stdbool.h>

#ifdef __cplusplus
extern "C"
{
#endif

  typedef enum
  {
    eSERIAL_PARITY_NONE,
    eSERIAL_PARITY_ODD,
    eSERIAL_PARITY_EVEN,
  } e_serial_parity;

  typedef struct SERIAL_MODE_STRUCT
  {
    int baud_rate;
    int data_bits;
    e_serial_parity parity;
    int stop_bits;
  } serial_mode_t;

  // Open a serial port
  int serial_begin(const char *port, serial_mode_t mode);
  // End a serial communication
  int serial_end(int fd);

  // Get the number of bytes (characters) available for reading from the serial
  // port
  int serial_available(int fd);

  // serial_write writes up to count bytes from the buffer starting at buf to the
  // file referred to by the file descriptor fd
  int serial_write(int fd, const void *buf, int count);

  // serial_read attempts to read up to count bytes from file descriptor fd into
  // the buffer starting at buf.
  int serial_read(int fd, void *buf, int count);

  // Set DTR status (Data Terminal Ready, pin 4)
  void set_DTR(int fd, bool status);
  // Get DTR status (Data Terminal Ready, pin 4)
  bool is_DTR(int fd);

  // Set RTS status (Request To Send, pin 7)
  void set_RTS(int fd, bool status);
  // Get RTS status (Request To Send, pin 7)
  bool is_RTS(int fd);

  // Get RI status (Ring Indicator, pin 9)
  bool is_RI(int fd);
  // Get DCD status (Data Carrier Detect, pin 1)
  bool is_DCD(int fd);
  // Get CTS status (Clear To Send, pin 8)
  bool is_CTS(int fd);
  // Get DSR status (Data Set Ready, pin 9)
  bool is_DSR(int fd);

  int serial_flush_reciever(int fd);
  int serial_flush_transmitter(int fd);
  int serial_flush(int fd);

#ifdef __cplusplus
}
#endif

#endif