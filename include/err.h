#ifndef CSERIAL_ERR_H
#define CSERIAL_ERR_H

#include <assert.h>

#ifdef __cplusplus
extern "C"
{
#endif

#define likely(x) __builtin_expect(!!(x), 1)
#define unlikely(x) __builtin_expect(!!(x), 0)

    void _error_check_failed(const char *file, int line, const char *expression)
        __attribute__((__noreturn__));
    void _error_check_failed_without_abort(const char *file, int line, const char *expression);

#define ERROR_CHECK(expression)                                   \
    do                                                            \
    {                                                             \
        if (unlikely((expression) != 0))                          \
        {                                                         \
            _error_check_failed(__FILE__, __LINE__, #expression); \
        }                                                         \
    } while (0)

#define ERROR_CHECK_WITHOUT_ABORT(expression)                               \
    do                                                                          \
    {                                                                           \
        if (unlikely((expression) != 0))                                        \
        {                                                                       \
            _error_check_failed_without_abort(__FILE__, __LINE__, #expression); \
        }                                                                       \
    } while (0)

#ifdef __cplusplus
}
#endif

#endif